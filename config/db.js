const mongoose = require('mongoose');

module.exports = mongoose.createConnection('mongodb://mongo/testme', {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
  useCreateIndex: true,
});
