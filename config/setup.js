const fs = require('fs');
const _ = require('lodash');
const path = require('path');

const { Logs } = require('../src/models');

const LOG_FILE = path.resolve(__dirname, '../misc/logs.txt');

const load = async () => {
  const rows = await Logs.countDocuments();

  console.log(rows);
  if (rows > 0)
    return {
      status: 200,
      msg: `Logs collection is already filled, ${rows} documents were found`,
    };
  else return await seeder();
};

const seeder = async () => {
  if (!fs.existsSync(LOG_FILE))
    return { status: 400, msg: 'Missing logs file' };

  const logFile = fs.readFileSync(LOG_FILE, { encoding: 'utf8' });

  const logArray = logFile
    .split('\n')
    .filter((i) => i.length > 0)
    .map((log) => {
      const parsed = JSON.parse(log);
      const dbObject = {
        consumerId: parsed?.authenticated_entity?.consumer_id?.uuid.trim(),
        serviceName: parsed?.service?.name.trim(),
        serviceHost: parsed?.service?.host.trim(),
        serviceId: parsed?.service?.id.trim(),
        latencies: parsed.latencies,
      };

      return dbObject;
    });

  const chunks = _.chunk(logArray, 1000);
  const totalChunks = chunks.length;

  let countProcessed = 0;

  console.log(`Generated ${totalChunks} chunks`);

  for (chunk of chunks) {
    await Logs.insertMany(chunk).then(() => {
      console.log(`Processed ${++countProcessed} of ${totalChunks}`);
    });
  }

  return { status: 200, msg: 'Data inserted.' };
};

module.exports = {
  load,
  seeder,
};
