# Teste Melhor Envio Backend

## Tecnologias utilizadas
- [NodeJS](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.com/)
- [Docker](https://www.docker.com/)

[Pacotes diversos](./package.json)


## Requisições
Há um arquivo JSON com as requisições na pasta [`misc/`](./misc), este arquivo pode ser importado na sua plataforma de APIs de preferência.

## Configuração inicial


* Este serviço roda a partir de containers, para iniciá-los é necessário ter instalado em sua máquina Docker e [Docker-Compose](https://docs.docker.com/compose/install/);

* Faça uma cópia e renomeie o arquivo `.env.copy` para `.env`.  
O conteúdo deste arquivo será enviado diretamente pelo desenvolvedor responsável pelo projeto;


* Após clonar o projeto, execute o comando `docker-compose up -d --build` na pasta raíz.

* Faça uma requisição para `http://localhost:8080/api/hello` para confirmar que o serviço foi iniciado corretamente.


## Carregando o arquivo de logs no banco de dados

Coloque o seu arquivo `logs.txt` dentro da pasta `/raiz-do-projeto/misc/`

Na pasta raíz, execute o comando `docker-compose logs -f api` para acompanhar o progresso e em seguida envie uma requisição para `htpp://localhost:8080/api/load`  
Esse processo deve levar alguns segundos para finalizar.  


Na primeira vez que uma requisição for feita para `/load` a resposta será **"Data inserted"**. Caso a requisição seja refeita, a resposta será **"Logs collection is already filled, XX documents were found"**, o número apresentado é calculado a partir dos registros no banco de dados.

## Gerando os relatórios
Url para geração de relatório: `http://localhost:8080/api/reports/{target}`.

### Relatórios
|Target|Relatório
|-|-|
`consumer`|Requisições por consumidor
`service`|Requisições por serviço
`average-time`|Tempo médio de request, proxy e gateway por serviço
||


Exemplo de requisição: `http://localhost:8080/api/reports/consumer`

Os relatórios serão gerados no formato .csv e armazenados dentro de `raiz-do-projeto/misc/reports/`