FROM node:14.16

WORKDIR /home

COPY package.json ./

RUN yarn global add nodemon
RUN yarn install

COPY . .

EXPOSE 4401

ENTRYPOINT npm start