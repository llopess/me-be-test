const { reports: service } = require('../services');

const generate = async (req, res) => {
  const query = await service[`${req.params.target}Report`]();
  return res.status(query.status).json(query.msg);
};

module.exports = {
  generate,
};
