const { Logs } = require('../models');
const { createFile } = require('../helpers');

const EMPTY_DB_ERROR = { status: 400, msg: 'Your database is empty' };

const getData = async () =>
  Logs.find({}).select(['-updatedAt', '-createdAt', '-__v']).lean();

const formatData = async (indexKey) => {
  const dbData = await getData();

  if (!dbData.length) return false;

  const indexedData = {};

  dbData.forEach((doc) => {
    if (indexedData[doc[indexKey]] && indexedData[doc[indexKey]].length) {
      indexedData[doc[indexKey]].push({
        ...doc,
        ...doc.latencies,
      });
    } else {
      indexedData[doc[indexKey]] = [{ ...doc, ...doc.latencies }];
    }
  });

  return indexedData;
};

const consumerReport = async () => {
  const consumers = await formatData('consumerId');
  if (!consumers) return EMPTY_DB_ERROR;

  let csvStruct =
    'CONSUMER,SERVICE ID,SERVICE NAME,SERVICE HOST,REQUEST (LATENCY),PROXY (LATENCY),GATEWAY (LATENCY)\n';

  for (key of Object.keys(consumers)) {
    consumers[key].forEach((c) => {
      csvStruct += `${c.consumerId},${c.serviceId},${c.serviceName},${c.serviceHost},${c.request},${c.proxy},${c.gateway}\n`;
    });
  }
  return createFile(csvStruct, 'consumer');
};

const serviceReport = async () => {
  const services = await formatData('serviceId');
  if (!services) return EMPTY_DB_ERROR;

  let csvStruct =
    'SERVICE ID,SERVICE NAME,SERVICE HOST,CONSUMER,REQUEST (LATENCY),PROXY (LATENCY),GATEWAY (LATENCY)\n';

  for (key of Object.keys(services)) {
    services[key].forEach((s) => {
      csvStruct += `${s.serviceId},${s.serviceName},${s.serviceHost},${s.consumerId},${s.request},${s.proxy},${s.gateway}\n`;
    });
  }

  return createFile(csvStruct, 'service');
};

const averageTimeReport = async () => {
  const services = await formatData('serviceId');
  if (!services) return EMPTY_DB_ERROR;

  const serviceTimes = [];
  for (key of Object.keys(services)) {
    const requestsPerService = services[key].length;
    const times = services[key].reduce(
      (prev, cur) => ({
        gateway: +prev.gateway + +cur.gateway,
        proxy: +prev.proxy + +cur.proxy,
        request: +prev.request + +cur.request,
      }),
      {
        gateway: 0,
        proxy: 0,
        request: 0,
      }
    );

    serviceTimes.push({
      service: services[key][0].serviceHost,
      gateway: (times.gateway / requestsPerService).toFixed(4),
      proxy: (times.proxy / requestsPerService).toFixed(4),
      request: (times.request / requestsPerService).toFixed(4),
      n: requestsPerService,
    });
  }

  let csvStruct =
    'SERVICE,REQUEST(AVG),PROXY(AVG),GATEWAY(AVG),TOTAL REQUESTS\n';
  csvStruct += serviceTimes
    .map((s) => `${s.service},${s.request},${s.proxy},${s.gateway},${s.n}`)
    .join('\n');

  return createFile(csvStruct, 'avgTime');
};

module.exports = {
  getData,
  consumerReport,
  serviceReport,
  'average-timeReport': averageTimeReport,
};
