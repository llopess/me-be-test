const router = require('express').Router();

const { reports: controller } = require('../controllers');

const ALLOWED_TARGETS = ['consumer', 'service', 'average-time'];

router.get(
  '/:target',
  (req, res, next) => {
    if (ALLOWED_TARGETS.includes(req.params.target)) next();
    else
      res.status(404).json({ err: 'Wrong path, please check documentation' });
  },
  controller.generate
);

module.exports = router;
