const fs = require('fs');
const path = require('path');
const moment = require('moment');

const { UID, GID } = process.env;

const REPORTS_DIR = path.resolve(__dirname, '../../misc/reports');

const reportPaths = {
  consumer: 'consumers-requests.csv',
  service: 'services-requests.csv',
  avgTime: 'services-average-time.csv',
};

const capitalize = (word) => {
  return word.slice(0, 1).toUpperCase() + word.slice(1, word.length);
};

const createRouting = (dir, indexFile, fn) => {
  const routes = {};

  fs.readdirSync(dir)
    .filter(
      (file) => file !== path.basename(indexFile) && file.slice(-3) === '.js'
    )
    .forEach((file) => {
      const current = require(path.join(dir, file));
      const filename = file.split('.js')[0];
      const index = fn ? fn(filename) : filename;
      routes[index] = current;
    });

  return routes;
};

const createFile = (data, report) => {
  const ts = moment().utcOffset(-3).format('YYYYMMDDHHmm');
  const reportFilePath = path.resolve(
    `${REPORTS_DIR}/${ts}_${reportPaths[report]}`
  );

  if (!fs.existsSync(REPORTS_DIR)) {
    fs.mkdirSync(REPORTS_DIR);
    fs.chownSync(REPORTS_DIR, parseInt(UID), parseInt(GID));
  }

  fs.writeFileSync(reportFilePath, data);
  fs.chownSync(reportFilePath, parseInt(UID), parseInt(GID));
  console.log(reportFilePath);

  if (fs.existsSync(reportFilePath))
    return { status: 200, msg: `Report ${ts}_${reportPaths[report]} created` };
  else return { status: 400, msg: `Something went wrong.` };
};

module.exports = {
  capitalize,
  createRouting,
  createFile,
};
