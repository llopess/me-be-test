const { createRouting, capitalize } = require('../helpers');
module.exports = createRouting(__dirname, __filename, capitalize);
