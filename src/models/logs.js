const { Schema } = require('mongoose');

const mongoDb = require('../../config/db');

const logSchema = new Schema(
  {
    consumerId: { type: String, required: true },
    serviceId: { type: String, required: true },
    serviceHost: { type: String, required: true },
    serviceName: { type: String, required: true },
    latencies: {
      gateway: { type: String },
      proxy: { type: String },
      request: { type: String },
    },
  },
  { timestamps: true }
);

module.exports = mongoDb.model('log', logSchema);
