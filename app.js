const express = require('express');

const setup = require('./config/setup');
const { reports } = require('./src/routes');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/hello', (req, res) => {
  const moment = require('moment');
  res.json({
    welcome: `Hello! It's ${moment()
      .utcOffset(-3)
      .format('DD/MM/YYYY HH:mm')} and your service is running.`,
  });
});
app.use('/load', async (req, res) => {
  const load = await setup.load();
  return res.status(load.status).json(load.msg);
});
app.use('/reports', reports);

app.listen(4401, async () => console.log('API serving on port 4401'));

module.exports = app;
